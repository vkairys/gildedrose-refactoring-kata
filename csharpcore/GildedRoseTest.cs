﻿using Xunit;
using System.Collections.Generic;
using csharpcore.ItemProcessors;

namespace csharpcore
{
    public class GildedRoseTest
    {
        [Fact]
        public void UpdateQuality_DecreasesQuality()
        {
            var (item, gildedRose) = CreateGildedRose(sellIn: 1, quality: 2);

            gildedRose.UpdateQuality();

            Assert.Equal(1, item.Quality);
        }

        [Fact]
        public void UpdateQuality_DecreasesSellIn()
        {
            var (item, gildedRose) = CreateGildedRose(sellIn: 2);

            gildedRose.UpdateQuality();

            Assert.Equal(1, item.SellIn);
        }

        [Fact]
        public void UpdateQuality_DecreasesQualityTwiceWhenSellByDateHasPassed()
        {
            var (item, gildedRose) = CreateGildedRose(sellIn: 0, quality: 3);

            gildedRose.UpdateQuality();

            Assert.Equal(1, item.Quality);
        }

        [Fact]
        public void UpdateQuality_DoesntMakeQualityNegative()
        {
            var (item, gildedRose) = CreateGildedRose(sellIn: 1, quality: 0);

            gildedRose.UpdateQuality();

            Assert.Equal(0, item.Quality);
        }

        [Fact]
        public void UpdateQuality_IncreasesSomeItemsQuality()
        {
            var (item, gildedRose) = CreateGildedRose(name: new AgedBrie().ItemName, sellIn:1, quality: 0);

            gildedRose.UpdateQuality();

            Assert.Equal(1, item.Quality);
        }

        [Fact]
        public void UpdateQuality_DoesntIncreaseQualityMoreThan50()
        {
            var (item, gildedRose) = CreateGildedRose(name: new AgedBrie().ItemName, quality: 50);

            gildedRose.UpdateQuality();
            Assert.Equal(50, item.Quality);
        }

        [Fact]
        public void UpdateQuality_DoesntSellLegendaryItems()
        {
            var (item, gildedRose) = CreateGildedRose(name: new Sulfuras().ItemName, sellIn: 1, quality: 80);

            gildedRose.UpdateQuality();
            Assert.Equal(1, item.SellIn);
            Assert.Equal(80, item.Quality);
        }

        [Fact]
        public void UpdateQuality_SupportsBackstagePasses()
        {
            var (item, gildedRose) = CreateGildedRose(name: new BackstagePasses().ItemName, sellIn: 10, quality: 0);

            gildedRose.UpdateQuality();

            Assert.Equal(2, item.Quality);
        }

        [Fact]
        public void UpdateQuality_SupportsConjured()
        {
            var (item, gildedRose) = CreateGildedRose(name: new Conjured().ItemName, sellIn: 1, quality: 3);

            gildedRose.UpdateQuality();

            Assert.Equal(1, item.Quality);
        }

        private static (Item, GildedRose) CreateGildedRose(string name = "foo", int sellIn = 0, int quality = 0)
        {
            var item = new Item { Name = name, SellIn = sellIn, Quality = quality };
            return (item, new GildedRose(new List<Item> { item }));
        }
    }
}