﻿using System;

namespace csharpcore.ItemProcessors
{
    public class BackstagePasses : IItemProcessor
    {
        public string ItemName { get; } = "Backstage passes";

        public void Process(Item item)
        {
            var quality = item.SellIn switch
            {
                var s when s <= 0 => 0,
                var s when s <= 5 => item.Quality + 3,
                var s when s <= 10 => item.Quality + 2,
                _ => item.Quality + 1
            };

            item.Quality = Math.Min(quality, Constants.MaxQuality);
            item.SellIn--;
        }
    }
}
