﻿
namespace csharpcore.ItemProcessors
{
    public interface IItemProcessor
    {
        string ItemName { get; }
        void Process(Item item);
    }
}
