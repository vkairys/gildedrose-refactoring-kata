﻿using System;

namespace csharpcore.ItemProcessors
{
    public class AgedBrie : IItemProcessor
    {
        public string ItemName { get; } = "Aged Brie";

        public void Process(Item item)
        {
            var increaseRate = item.SellIn > 0 ? 1 : 2;
            item.Quality = Math.Min(item.Quality + increaseRate, Constants.MaxQuality);
            item.SellIn--;
        }
    }
}
