﻿using System;

namespace csharpcore.ItemProcessors
{
    public class Conjured : IItemProcessor
    {
        public string ItemName { get; } = "Conjured";

        public void Process(Item item)
        {
            item.Quality = Math.Min(item.Quality - 2, Constants.MaxQuality);
            item.SellIn--;
        }
    }
}
