﻿using csharpcore.ItemProcessors;
using Xunit;

namespace csharpcore
{
    public class BackstagePassesTests
    {
        public static BackstagePasses BackstagePasses = new BackstagePasses();

        [Fact]
        public void Process_IncreasesQuality()
        {
            var item = new Item
            {
                SellIn = 11,
                Quality = 1
            };
            BackstagePasses.Process(item);

            Assert.Equal(2, item.Quality);
        }

        [Fact]
        public void Process_IncreasesQualityBy2WhenSellInLessThan11()
        {
            var item = new Item
            {
                SellIn = 10,
                Quality = 1
            };
            BackstagePasses.Process(item);

            Assert.Equal(3, item.Quality);
        }

        [Fact]
        public void Process_IncreasesQualityBy3WhenSellInLessThan5()
        {

            var item = new Item
            {
                SellIn = 5,
                Quality = 1
            };
            BackstagePasses.Process(item);

            Assert.Equal(4, item.Quality);
        }

        [Fact]
        public void Process_ChangesQualityToZeroAfterConcert()
        {
            var item = new Item
            {
                SellIn = 0,
                Quality = 1
            };
            BackstagePasses.Process(item);

            Assert.Equal(0, item.Quality);
        }
    }
}
