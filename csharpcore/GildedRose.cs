﻿using System;
using System.Collections.Generic;
using System.Linq;
using csharpcore.ItemProcessors;

namespace csharpcore
{
    public class GildedRose
    {
        private readonly IItemProcessor[] _mappers;

        IList<Item> Items;
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
            _mappers = new IItemProcessor[] { new BackstagePasses(), new Sulfuras(), new AgedBrie(), new Conjured() };
        }

        public void UpdateQuality()
        {
            foreach (var item in Items)
            {
                var processor = FindProcessor(item.Name);

                if (processor != null)
                {
                    processor.Process(item);
                }
                else
                {
                    item.Quality = item.SellIn > 0
                        ? item.Quality - 1
                        : item.Quality - 2;
                    item.SellIn--;
                }

                if (item.Quality < Constants.MinQuality)
                {
                    item.Quality = Constants.MinQuality;
                }
            }
        }

        private IItemProcessor FindProcessor(string name) => _mappers.FirstOrDefault(x =>
            name.IndexOf(x.ItemName, StringComparison.OrdinalIgnoreCase) >= 0);
    }
}
