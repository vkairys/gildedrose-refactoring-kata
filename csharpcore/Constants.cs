﻿namespace csharpcore
{
    public static class Constants
    {
        public static int MaxQuality = 50;
        public static int MinQuality = 0;
    }
}
